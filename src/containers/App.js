import React, { Component } from 'react';
import Particles from 'react-particles-js';

import Navigation from '../components/Navigation/Navigation';
import Logo from '../components/Logo/Logo';
import ImageLinkForm from '../components/ImageLinkForm/ImageLinkForm';
import Rank from '../components/Rank/Rank';
import FaceDetection from '../components/FaceDetection/FaceDetection';
import SignIn from '../components/SignIn/SignIn';
import Register from '../components/Register/Register';

import particlesOptions from './particlesjs-config.json';
import './App.css';

const initialState = {
  input: '',
  imageUrl: '',
  box: {},
  route: 'signin',
  isSignedIn: false,
  user: {
    id: '',
    name: '',
    entries: 0,
    joined: ''
  }
}
class App extends Component {
  state = initialState;

  loadUser = (data) => {
    this.setState({
      user: {
        id: data.id,
        name: data.name,
        entries: data.entries,
        joined: data.joined
      }
    })
  }

  onInputChange = (event) => {
    this.setState({input: event.target.value});
  }

  calculateFaceLoc = (data) => {
    const face = data.outputs[0].data.regions[0].region_info.bounding_box;
    const image = document.getElementById('input-image');
    const width = Number(image.width);
    const height = Number(image.height);

    return {
      leftCol: face.left_col * width,
      topRow: face.top_row * height,
      rightCol: width - (face.right_col * width),
      bottomRow: height - (face.bottom_row * height)
    }
  }

  displayBox = (box) => {
    this.setState({ box });
  }

  onButtonSubmit = () => {
    this.setState(() => {
      return {imageUrl: this.state.input}
    });

    fetch('https://blooming-refuge-15532.herokuapp.com/imageurl', {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        input: this.state.input
      })
    })
    .then(res => res.json())
    .then(response => {
      if (response) {
        fetch('https://blooming-refuge-15532.herokuapp.com/image', {
          method: 'put',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
            id: this.state.user.id
          })
        })
          .then(response => response.json())
          .then(count => {
            this.setState(Object.assign(this.state.user, { entries: count }));
          })
          .catch(console.log);
      }

      this.displayBox(this.calculateFaceLoc(response))
    })
    .catch(console.log);
  }

  onRouteChange = (route) => {
    if (route === 'signout') {
      this.setState(initialState);
    } else if (route === 'home') {
      this.setState({ isSignedIn: true });      
    }

    this.setState({ route });
  }

  render() {
    const { isSignedIn, imageUrl, route, box } = this.state;
    return (
      <div className="App">

        <Particles className="particles" params={particlesOptions} />
        <Navigation
          onRouteChange={this.onRouteChange}
          isSignedIn={isSignedIn}
        />

        { route === 'home'
          ? (<div>
              <Logo />
              <Rank 
                name={this.state.user.name}
                entries={this.state.user.entries}
              />
              <ImageLinkForm 
                onInputChange={this.onInputChange}
                onButtonSubmit={this.onButtonSubmit}
              />
              <FaceDetection imageUrl={imageUrl} box={box} />
            </div>
          ) : (
            route === 'signin'
            ? <SignIn
                onRouteChange={this.onRouteChange}
                loadUser={this.loadUser}
              />
            : <Register
                onRouteChange={this.onRouteChange}
                loadUser={this.loadUser}
              />
          )
        }
      </div>
    );
  }
}

export default App;
