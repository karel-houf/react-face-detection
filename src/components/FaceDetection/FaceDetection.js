import React from "react";
import './FaceDetection.css'

const FaceDetection = ({ imageUrl, box }) => {
  return (
    <div className="center-flex ma">
      <div className="absolute mt2">
        <img
          id="input-image"
          src={imageUrl}
          alt="for face detecion"
          width="500px"
          height="auto"
        />
        <div
          className="bouding-box"
          style={
            {
              top: box.topRow,
              right: box.rightCol,
              bottom: box.bottomRow,
              left: box.leftCol,
            }
          }
        ></div>
      </div>
    </div>
  );
};

export default FaceDetection;
